import React, { Component } from 'react';
import './Country.css';

class Country extends Component {
  shouldComponentUpdate(newProps) {
    return (newProps.className !== this.props.className);
  }

  render() {
    return (
      <span
        className={this.props.className}
        onClick={() => this.props.click(this.props.country, true)}
      >{this.props.country}</span>
    );
  }
}

export default Country;