import React, { Component } from 'react';
import './CountriesList.css';
import Country from "./Country/Country";
import Loader from "../UI/Loader/Loader";

class CountriesList extends Component {
  getOffsetSum = (elem) => {
    let top = 0;
    let left = 0;

    while(elem) {
      top = top + parseInt(elem.offsetTop, 10);
      left = left + parseInt(elem.offsetLeft, 10);
      elem = elem.offsetParent;
    }

    return {top: top, left: left};
  };

  componentDidUpdate(newProps) {
    if (!this.props.onList && newProps.onList) {
      const elem = document.getElementsByClassName('Selected');
      const coordinates = this.getOffsetSum(elem[0]);
      this.refs.Countries.scrollTo(0, coordinates.top - 45);
    }
  };

  render() {
    return (
      <div className="CountriesList" ref="Countries">
        {this.props.isListLoading ? <Loader /> : null}
        {this.props.countries.map((country, index) => {
          let addClassName = 'Country';
          if (this.props.selected === country) addClassName += ' Selected';

          return (
            <Country
              key={index}
              className={addClassName}
              country={country}
              click={this.props.click}
            />
          )}
        )}
      </div>
    );
  }
}

export default CountriesList;