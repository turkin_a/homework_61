import React, { Component, Fragment } from 'react';
import './CountryData.css';

class CountryData extends Component {
  render() {
    return (
      <Fragment>
        <div className="CountryFlag">
          <img src={this.props.country.flag} alt="" title={this.props.country.name}/>
        </div>
        <div className="CountryName">{this.props.country.name}</div>
        <div className="CountryData"><strong>Capital: </strong>{this.props.country.capital}</div>
        <div className="CountryData"><strong>Region: </strong>{this.props.country.region}</div>
        <div className="CountryData"><strong>Population: </strong>{this.props.country.population}</div>
        <div className="CountryData"><strong>Area: </strong>{this.props.country.area}</div>
        <div className="CountryBorders"><strong>Borders with:</strong>
          <ul>
            {this.props.borders.length === 0 ? 'Have not borders' :
              (this.props.borders.map(border => <li key={border}>
              <span onClick={() => this.props.click(border, false)}>{border}</span></li>))}
          </ul>
        </div>
      </Fragment>
    );
  }
}

export default CountryData;