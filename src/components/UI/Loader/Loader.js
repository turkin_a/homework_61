import React, { Component } from 'react';
import './Loader.css';

class CountryData extends Component {
  render() {
    return (
      <div className="Loader">
        <img src="./img/preloader.svg" alt=""/>
      </div>
    );
  }
}

export default CountryData;